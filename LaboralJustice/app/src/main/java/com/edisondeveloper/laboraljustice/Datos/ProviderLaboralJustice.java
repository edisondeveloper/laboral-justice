package com.edisondeveloper.laboraljustice.Datos;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.edisondeveloper.laboraljustice.Datos.ContratoLaboralJustice.*;

public class ProviderLaboralJustice extends ContentProvider {

    private DBlaboralJustice dataBaseLaboralJustice;
    private static UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private static final int TABLE_CODE = 100;
    private static final int ITEM_CODE = 101;

    static{
        uriMatcher.addURI(ContratoLaboralJustice.CONTENT_AUTHORITY, ContratoLaboralJustice.TABLE_NAME, TABLE_CODE);
        uriMatcher.addURI(ContratoLaboralJustice.CONTENT_AUTHORITY, ContratoLaboralJustice.TABLE_NAME + "/#", ITEM_CODE);
    }

    public ProviderLaboralJustice(){
        super();
    }

    @Override
    public boolean onCreate() {
        dataBaseLaboralJustice = new DBlaboralJustice(getContext());
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase dataBase = dataBaseLaboralJustice.getReadableDatabase();
        Cursor cursor = null;
        int typeMatch = uriMatcher.match(uri);
        switch(typeMatch){
            case TABLE_CODE:
                cursor = dataBase.query(TablaCasos.TABLE_NAME, projection, selection, selectionArgs, null, null, null);
                break;
            case ITEM_CODE:
                selection = TablaCasos.COLUMNA_ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = dataBase.query(TablaCasos.TABLE_NAME, projection, selection, selectionArgs, null, null, null, null);
                break;
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        int typeMatch = uriMatcher.match(uri);
        switch (typeMatch){
            case TABLE_CODE:
                return ContratoLaboralJustice.CONTENT_LIST_TYPE;
            case ITEM_CODE:
                return ContratoLaboralJustice.CONTENT_LIST_TYPE;
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SQLiteDatabase database = dataBaseLaboralJustice.getWritableDatabase();
        long idFila = -1;
        int typeMath = uriMatcher.match(uri);
        switch (typeMath){
            case TABLE_CODE:
                idFila = database.insert(TablaCasos.TABLE_NAME, null, values);
                break;
            default:
                System.out.println("Error al intentar insertar datos en la tabla casos");
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return ContentUris.withAppendedId(uri, idFila);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase database = dataBaseLaboralJustice.getWritableDatabase();
        int rowsDelete = 0;
        int typeMatch = uriMatcher.match(uri);
        switch (typeMatch){
            case TABLE_CODE:
                rowsDelete = database.delete(TablaCasos.TABLE_NAME, selection, selectionArgs);
                break;
            case ITEM_CODE:
                selection = TablaCasos.COLUMNA_ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsDelete = database.delete(TablaCasos.TABLE_NAME, selection, selectionArgs);
                break;
        }

        if (rowsDelete != 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsDelete;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase database = dataBaseLaboralJustice.getWritableDatabase();
        int typeMatch = uriMatcher.match(uri);
        int rowsUpdates = 0;
        switch (typeMatch){
            case TABLE_CODE:
                rowsUpdates = database.update(TablaCasos.TABLE_NAME, values, selection, selectionArgs);
                break;
            case ITEM_CODE:
                selection = TablaCasos.COLUMNA_ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsUpdates = database.update(TablaCasos.TABLE_NAME,values, selection, selectionArgs);
                break;
        }

        if(rowsUpdates != 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsUpdates;
    }
}
