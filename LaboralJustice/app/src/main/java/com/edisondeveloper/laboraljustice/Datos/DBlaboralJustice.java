package com.edisondeveloper.laboraljustice.Datos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.edisondeveloper.laboraljustice.Datos.ContratoLaboralJustice.*;

public class DBlaboralJustice extends SQLiteOpenHelper {

    private static final String NAME_DB = "casosInjustos.db";
    private static final int VERSION_DB = 1;

    public DBlaboralJustice(Context context){
        super(context, NAME_DB, null, VERSION_DB);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String STATEMENT_CREATE_TABLE = "CREATE TABLE " + TablaCasos.TABLE_NAME + " ("
                + TablaCasos.COLUMNA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TablaCasos.COLUMNA_TIPO + " TEXT NOT NULL, " +
                TablaCasos.COLUMNA_DESCRIPCION + " TEXT NOT NULL, " +
                TablaCasos.COLUMNA_SOLUCION + " TEXT NOT NULL);";
        db.execSQL(STATEMENT_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
