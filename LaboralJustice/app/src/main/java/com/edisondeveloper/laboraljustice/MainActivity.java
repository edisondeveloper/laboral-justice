package com.edisondeveloper.laboraljustice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.edisondeveloper.laboraljustice.Datos.AdapterListCases;
import com.edisondeveloper.laboraljustice.Datos.IndividualCaseList;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterListCases.OnClickItemListener {

    private RecyclerView recyclerView;
    private AdapterListCases adapter;
    private ArrayList<IndividualCaseList> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView = findViewById(R.id.list_justice_cases);
        list = new ArrayList<>();
        list.add(new IndividualCaseList(getString(R.string.case_one), R.drawable.previous_image));
        list.add(new IndividualCaseList(getString(R.string.case_two), R.drawable.previous_image));
        list.add(new IndividualCaseList(getString(R.string.case_three), R.drawable.previous_image));
        list.add(new IndividualCaseList(getString(R.string.case_four), R.drawable.previous_image));
        list.add(new IndividualCaseList(getString(R.string.case_five), R.drawable.previous_image));
        list.add(new IndividualCaseList(getString(R.string.case_six), R.drawable.previous_image));
        list.add(new IndividualCaseList(getString(R.string.case_seven), R.drawable.previous_image));
        adapter = new AdapterListCases(this, list, this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onListItemClick(int positionClicked) {
        Intent detailsCase = new Intent(this, DetailsCaseActivity.class);
        startActivity(detailsCase);
    }
}
