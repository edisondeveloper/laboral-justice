package com.edisondeveloper.laboraljustice.Datos;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.edisondeveloper.laboraljustice.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterListCases extends RecyclerView.Adapter<AdapterListCases.ViewHolderCases> {

    private List<IndividualCaseList> listObjects;
    private Context context;
    private OnClickItemListener onClickItemListener;

    public AdapterListCases(Context contextp, ArrayList<IndividualCaseList> list, OnClickItemListener onClickItemListenerp){
        listObjects = list;
        context = contextp;
        onClickItemListener = onClickItemListenerp;
    }

    @NonNull
    @Override
    public ViewHolderCases onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_cases,viewGroup, false);
        return new ViewHolderCases(view);
    }

    @Override
    public int getItemCount() {
        return listObjects.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderCases viewHolderCases, int i) {
        IndividualCaseList individualCase = listObjects.get(i);
        viewHolderCases.getTextView().setText(individualCase.getTitle());
        viewHolderCases.getImageView().setImageResource(individualCase.getImageCase());
    }

    public interface OnClickItemListener{
        void onListItemClick(int positionClicked);
    }

    public class ViewHolderCases extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView textView;
        private ImageView imageView;

        public ViewHolderCases(View rootView){
            super(rootView);
            textView = rootView.findViewById(R.id.title_case);
            imageView = rootView.findViewById(R.id.image_case);
            rootView.setOnClickListener(this);
        }

        public TextView getTextView() {
            return textView;
        }

        public void setTextView(TextView textView) {
            this.textView = textView;
        }

        public ImageView getImageView() {
            return imageView;
        }

        public void setImageView(ImageView imageView) {
            this.imageView = imageView;
        }

        @Override
        public void onClick(View v) {
            int clickPosition = getAdapterPosition();
            onClickItemListener.onListItemClick(clickPosition);
        }
    }

}
