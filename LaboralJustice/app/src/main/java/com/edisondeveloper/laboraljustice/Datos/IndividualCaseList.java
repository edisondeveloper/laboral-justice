package com.edisondeveloper.laboraljustice.Datos;

public class IndividualCaseList {

    private String title;
    private int imageCase;

    public IndividualCaseList(String titlep, int imageCasep){
        title = titlep;
        imageCase = imageCasep;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageCase() {
        return imageCase;
    }

    public void setImageCase(int imageCase) {
        this.imageCase = imageCase;
    }
}
