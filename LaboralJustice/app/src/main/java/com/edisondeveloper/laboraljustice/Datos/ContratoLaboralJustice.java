package com.edisondeveloper.laboraljustice.Datos;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class ContratoLaboralJustice {

    public static final String CONTENT_AUTHORITY = "com.edisondeveloper.laboraljustice";
    public static final String PATCH = "casos";
    public static final String TABLE_NAME = "casos";
    public static final Uri BASE_CONTENT_AUTHORITY = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATCH;
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATCH;

    public static class TablaCasos implements BaseColumns{

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_AUTHORITY, PATCH);
        public static final String TABLE_NAME = "casos";

        public static final String COLUMNA_ID = BaseColumns._ID;
        public static final String COLUMNA_TIPO = "tipo";
        public static final String COLUMNA_DESCRIPCION = "descripcion";
        public static final String COLUMNA_SOLUCION = "solucion";

    }

}
